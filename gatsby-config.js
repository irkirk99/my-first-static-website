module.exports = {
  siteMetadata: {
    siteUrl: "https://www.yourdomain.tld",
    title: "My First static website",
  },
  plugins: ["gatsby-plugin-gatsby-cloud"],
};
